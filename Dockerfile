FROM debian:bullseye-slim

ARG MUNGE_VER
ARG LIBEVENT_VER
ARG HWLOC_VER
ARG HWLOC_VER_ABREV
ARG PMIX_VER
ARG SLURM_VER
ARG OPENMPI_VER
ARG OPENMPI_VER_ABREV

RUN apt-get update && \
    apt-get -y install gcc g++ gfortran libgcrypt20-dev libncurses5-dev make python3 python python3-pip wget xz-utils bzip2 && \
    rm -rf /var/lib/apt/lists/* && \
    \
    # Optain Munge, Slurm and OpenMPI \
    wget -O /root/munge-${MUNGE_VER}.tar.xz https://github.com/dun/munge/releases/download/munge-${MUNGE_VER}/munge-${MUNGE_VER}.tar.xz && \
    wget -O /root/libevent-${LIBEVENT_VER}.tar.gz https://github.com/libevent/libevent/releases/download/release-${LIBEVENT_VER}/libevent-${LIBEVENT_VER}.tar.gz && \
    wget -O /root/hwloc-${HWLOC_VER}.tar.gz https://download.open-mpi.org/release/hwloc/${HWLOC_VER_ABREV}/hwloc-${HWLOC_VER}.tar.gz && \
    wget -O /root/pmix-${PMIX_VER}.tar.gz https://github.com/openpmix/openpmix/releases/download/v${PMIX_VER}/pmix-${PMIX_VER}.tar.gz && \
    wget -O /root/slurm-${SLURM_VER}.tar.gz https://github.com/SchedMD/slurm/archive/slurm-${SLURM_VER}.tar.gz && \
    wget -O /root/openmpi-${OPENMPI_VER}.tar.bz2 https://www.open-mpi.org/software/ompi/${OPENMPI_VER_ABREV}/downloads/openmpi-${OPENMPI_VER}.tar.bz2 && \
    \
    # Install Munge, Slurm, OpenPMIx and OpenMPI, and remove source \
    mkdir -p /root/local/src && \
    \
    cd /root/local/src && tar axvf /root/munge-${MUNGE_VER}.tar.xz && cd /root/local/src/munge-${MUNGE_VER} && \
    ./configure --prefix=/usr/local && \
    make -j && make install && \
    rm -rf /root/local/src/munge-${MUNGE_VER} /root/munge-${MUNGE_VER}.tar.xz && \
    \
    cd /root/local/src && tar axvf /root/libevent-${LIBEVENT_VER}.tar.gz && cd /root/local/src/libevent-${LIBEVENT_VER} && \
    ./configure --prefix=/usr/local --disable-openssl && \
    make -j && make install && \
    rm -rf /root/local/src/libevent-${LIBEVENT_VER} /root/libevent-${LIBEVENT_VER}.tar.gz && \
    \
    cd /root/local/src && tar axvf /root/hwloc-${HWLOC_VER}.tar.gz && cd /root/local/src/hwloc-${HWLOC_VER} && \
    ./configure --prefix=/usr/local && \
    make -j && make install && \
    rm -rf /root/local/src/hwloc-${HWLOC_VER} /root/hwloc-${HWLOC_VER}.tar.gz && \
    \
    cd /root/local/src && tar axvf /root/pmix-${PMIX_VER}.tar.gz && cd /root/local/src/pmix-${PMIX_VER} && \
    ./configure --prefix=/usr/local --with-libevent=/usr/local --with-hwloc=/usr/local && \
    make -j && make install && \
    rm -rf /root/local/src/pmix-${PMIX_VER} /root/pmix-${PMIX_VER}.tar.gz

RUN cd /root/local/src && tar axvf /root/slurm-${SLURM_VER}.tar.gz && cd /root/local/src/slurm-slurm-${SLURM_VER} && \
    ./configure --help && \
    ./configure --prefix=/usr/local --with-pmix=/usr/local && \
    make -j && make install && \
    rm -rf /root/local/src/slurm-slurm-${SLURM_VER} /root/slurm-${SLURM_VER}.tar.gz

RUN cd /root/local/src && tar axvf /root/openmpi-${OPENMPI_VER}.tar.bz2 && cd /root/local/src/openmpi-${OPENMPI_VER} && \
    ./configure --help && \
    ./configure --prefix=/usr/local --with-pmi=/usr/local && \
    make -j && make install && \
    rm -rf /root/local/src/openmpi-${OPENMPI_VER} /root/openmpi-${OPENMPI_VER}.tar.bz2 && \
    echo 'btl_tcp_if_exclude = lo,docker0' >> /usr/local/etc/openmpi-mca-params.conf && \
    cp /usr/local/lib/libmpi_usempif08.so.40 /usr/lib/libmpi_usempi.so.40 && ldconfig  && \
    useradd munge -m && \
    useradd slurm -m && \
    mkdir /tmp/slurm && \
    chown slurm:slurm -R /tmp/slurm && \
    \
    apt-get remove --purge --autoremove -y python gcc g++ gfortran make libgcrypt20-dev libncurses5-dev && \
    apt-get clean

COPY scripts/munged.sh scripts/slurm-config.sh scripts/leader.sh /scripts/
COPY config/slurm.conf.template /usr/local/etc/

