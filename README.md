# Docker Debian SLURM

[![Docker Pulls](https://img.shields.io/docker/pulls/theowoo/debian-slurm.svg)](https://hub.docker.com/r/theowoo/debian-slurm) [![pipeline status](https://gitlab.mpcdf.mpg.de/woot/docker-debian-slurm/badges/master/pipeline.svg)](https://gitlab.mpcdf.mpg.de/woot/docker-debian-slurm/commits/master)

[![](https://images.microbadger.com/badges/image/theowoo/debian-slurm.svg)](https://microbadger.com/images/theowoo/debian-slurm) [![](https://images.microbadger.com/badges/version/theowoo/debian-slurm.svg)](https://microbadger.com/images/theowoo/debian-slurm)

This is the repository generates a docker image that provide SLURM in Debian. It contains the dependencies `Munge` and `OpenMPI`, and in addition `wget`. (More detailed usage instructions can be found in the parent repository written for Ubuntu: [jamesmcclain/SlurmDocker](https://github.com/jamesmcclain/SlurmDocker))

# Obtaining

The image can be pulled from [DockerHub](https://hub.docker.com/r/theowoo/debian-slurm/).
```bash
docker pull theowoo/debian-slurm:latest
```

# Gitlab CI

To use this image for [Gitlab CI](https://docs.gitlab.com/ee/ci/) with docker-in-docker (e.g. when using a shared Gitlab runner in a [non-privileged mode](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor)), make use of `artifacts` to [pass the build](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html) from the SLURM pipeline job to your main job. Note the addition of `dependencies` and `before_script`.
```yaml
slurm:
  stage: build
  image:
    name: theowoo/debian-slurm:latest
  script:
    - mkdir slurm_local
    - cp -R /usr/local/* slurm_local/
    - mkdir slurm_scripts
    - cp -R /scripts/* slurm_scripts/
  artifacts:
    expire_in: 1 hr
    paths:
      - slurm_local/
      - slurm_scripts/

main:
  stage: test
  image: 
    name: debian:latest
  dependencies:
    - slurm
  before_script:
    - cp -r slurm_local/* /usr/local/ && rm -r slurm_local/
    - mkdir -p /scripts
    - cp -r slurm_scripts/* /scripts/ && rm -r slurm_scripts/
    - useradd munge -m
    - useradd slurm -m
    - mkdir -p /tmp/slurm 
    - chown slurm:slurm -R /tmp/slurm
    - /scripts/leader.sh $HOSTNAME $HOSTNAME CPUs=$(nproc)
  script:
    - sinfo Nl
    - srun --mpi=pmi2 -n $(nproc) env | grep SLURM_PROCID
```

To use a custom [SLURM config file](https://slurm.schedmd.com/slurm.conf.html), do `cp custom-slurm.conf /usr/local/etc/slurm.conf.template` before `/scripts/leader.sh`

# License

The contents of this repository are covered under the [MIT License](LICENSE.md).
The actual pieces of software (Slurm, MUNGE, &c) belong to their respective owners and covered by their respective licenses.
